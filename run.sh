#!/bin/sh

ansible-playbook app.yml --connection=local \
  --inventory=environments/development/inventory
